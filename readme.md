## Todo list

This is a simple todo list project created for demonstration. The application was developed using a three-layered architecture. Web application is presented as WebAPI service.

## Todo list database

To start using this application you need to publish related database. Database is stored as SQL project in src/ToDoListDb. To give access to database to todo list application you need to specify connection string which is located in src/ToDoList/ToDoList.WebApi/appsettings.json.