﻿using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer.Contexts
{
    internal class ToDoListContext : DbContext
    {
        private readonly string _connectionString;

        public ToDoListContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }

        public DbSet<Models.Task> Tasks { get; set; }
    }
}
