﻿using Autofac;
using DataAccessLayer.Contexts;
using DataAccessLayer.Repositories;

namespace DataAccessLayer
{
    public class DataAccessModule : Module
    {
        public string ConnectionString { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ToDoListContext>()
                .WithParameter(new TypedParameter(typeof(string), ConnectionString));

            builder.RegisterType<TaskRepository>().As<IRepository<Models.Task>>();
        }
    }
}
