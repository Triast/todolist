﻿namespace DataAccessLayer.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsCompleted { get; set; }
    }
}
