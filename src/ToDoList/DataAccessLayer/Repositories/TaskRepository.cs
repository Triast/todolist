﻿using DataAccessLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer.Repositories
{
    internal class TaskRepository : IRepository<Task>
    {
        private readonly Contexts.ToDoListContext _ctx;

        public TaskRepository(Contexts.ToDoListContext ctx)
        {
            _ctx = ctx;
        }

        public int Add(Task entity)
        {
            var entry = _ctx.Tasks.Add(entity);
            _ctx.SaveChanges();

            return entry.Entity.Id;
        }

        public void AddRange(IEnumerable<Task> entities)
        {
            _ctx.Tasks.AddRange(entities);
            _ctx.SaveChanges();
        }

        public Task Get(int id) => _ctx.Tasks.Find(id);

        public IEnumerable<Task> GetAll() => _ctx.Tasks.AsEnumerable();

        public void Remove(Task entity)
        {
            var task = _ctx.Tasks.Find(entity.Id);
            _ctx.Tasks.Remove(task);
            _ctx.SaveChanges();
        }

        public void RemoveRange(IEnumerable<Task> entities)
        {
            _ctx.Tasks.RemoveRange(entities);
            _ctx.SaveChanges();
        }

        public void Update(Task entity)
        {
            var task = _ctx.Tasks.Find(entity.Id);
            _ctx.Entry(task).CurrentValues.SetValues(entity);
            _ctx.SaveChanges();
        }
    }
}
