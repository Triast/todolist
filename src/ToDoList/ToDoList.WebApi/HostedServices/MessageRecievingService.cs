using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogicLayer.DataTransferObjects;
using BusinessLogicLayer.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using ToDoList.WebApi.Controllers;
using ToDoList.WebApi.Helper;

namespace ToDoList.WebApi.HostedServices
{
    public class MessageRecievingService : IHostedService
    {
        private readonly ITaskService _taskService;

        private readonly IConnection connection;
        private readonly IModel channel;
        private readonly EventingBasicConsumer consumer;

        private readonly string _queueName;

        public MessageRecievingService(ITaskService taskService, IConfiguration configuration, IMessageSender sender)
        {
            _taskService = taskService;
            _queueName = configuration.GetValue<string>("ServiceId");

            var factory = new ConnectionFactory() { HostName = "localhost" };

            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var message = JsonConvert.DeserializeObject<TaskMessage>(Encoding.UTF8.GetString(ea.Body));
                var task = JsonConvert.DeserializeObject<TaskDto>(message.Data);

                switch (message.Action)
                {
                    case "POST":
                        var id = _taskService.Add(task);
                        task.Id = id;
                        sender.Add(task);
                        break;
                    case "PATCH":
                        _taskService.Complete(task.Id);
                        break;
                    case "DELETE":
                        _taskService.Remove(task.Id);
                        break;
                }
            };
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            channel.QueueDeclare(queue: _queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            channel.BasicConsume(queue: _queueName,
                                autoAck: true,
                                consumer: consumer);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            connection.Close();

            return Task.CompletedTask;
        }
    }
}