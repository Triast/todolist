﻿using BusinessLogicLayer.DataTransferObjects;
using BusinessLogicLayer.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketManagement.VenueAndUserManagement.Helpers;
using ToDoList.WebApi.Helper;

namespace ToDoList.WebApi.Controllers
{
    /// <summary>
    /// Controller for handling CRUD operations with tasks.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;
        private readonly IMessageSender _messageSender;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Constructor which accepts task service.
        /// </summary>
        public TasksController(ITaskService taskService, IMessageSender messageSender, IConfiguration configuration)
        {
            _taskService = taskService;
            _messageSender = messageSender;
            _configuration = configuration;
        }

        /// <summary>
        /// Returns all tasks.
        /// </summary>
        /// <response code="200">Returned all tasks as collection.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpGet]
        public ActionResult<List<TaskDto>> Get()
        {
            return Ok(_taskService.GetAll().ToList());
        }

        /// <summary>
        /// Returns task with specified id.
        /// </summary>
        /// <param name="id">
        /// Integer id of task.
        /// </param>
        /// <response code="200">Returned task with specified id.</response>
        /// <response code="400">There is no task with provided id.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpGet("{id}")]
        public ActionResult<TaskDto> Get([FromRoute] int id)
        {
            var task = _taskService.Get(id);

            if (task == null)
            {
                return BadRequest();
            }

            return Ok(task);
        }

        /// <summary>
        /// Creates new task.
        /// </summary>
        /// <param name="task">
        /// Task to add.
        /// </param>
        /// <response code="200">New task is created and id is returned.</response>
        /// <response code="400">Some validation error occured.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpPost]
        public ActionResult<int> Post([FromBody] TaskDto task)
        {
            try
            {
                var id = _taskService.Add(task);

                task.Id = id;
                _messageSender.Add(task);

                return Ok(id);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Sets competeness of task.
        /// </summary>
        /// <param name="id">
        /// Id of task to be completed.
        /// </param>
        /// <response code="200">Task is completed and returned.</response>
        /// <response code="400">Some validation errors occured.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpPatch("{id}")]
        public ActionResult<TaskDto> Patch(int id)
        {
            try
            {
                _taskService.Complete(id);
                var task = _taskService.Get(id);

                _messageSender.Update(task);

                return Ok(task);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deletes task.
        /// </summary>
        /// <param name="id">
        /// Task to be deleted.
        /// </param>
        /// <response code="200">Taks successfully deleted.</response>
        /// <response code="400">Some validation errors occured.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var task = _taskService.Get(id);
                _taskService.Remove(id);

                _messageSender.Remove(task);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [Route("register")]
        [HttpPost]
        public async Task<ActionResult> Register()
        {
            var serviceId = _configuration.GetValue<string>("ServiceId");
            var serviceName = _configuration.GetValue<string>("ServiceName");
            var managerAddress = _configuration.GetValue<string>("ManagerAddress");

            var request = new CustomHttpRequest(managerAddress);

            var message = new RegisterMessage
            {
                Id = serviceId,
                Name = serviceName,
                Tasks = _taskService.GetAll()
            };

            await request.PostRequest(message, "api/Services");

            return Ok();
        }
    }
}
