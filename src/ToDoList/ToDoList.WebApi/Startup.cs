﻿using Autofac;
using BusinessLogicLayer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;
using ToDoList.WebApi.Helper;
using ToDoList.WebApi.HostedServices;
using ToDoList.WebApi.Middleware;

namespace ToDoList.WebApi
{
    /// <summary>
    /// Class where dependecy injection and http request is configured.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Constructor :).
        /// </summary>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Configuration :).
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(setup =>
            {
                setup.SwaggerDoc("v1", new Info { Title = "ToDoList API", Version = "v1" });

                var filePath = Path.Combine(System.AppContext.BaseDirectory, "ToDoList.WebApi.xml");
                setup.IncludeXmlComments(filePath);
            });

            services.AddHostedService<MessageRecievingService>();

            services.AddMvc(options => options.RespectBrowserAcceptHeader = true)
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddXmlSerializerFormatters();
        }

        /// <summary>
        /// ConfigureContainer is where you can register things directly
        /// with Autofac. This runs after ConfigureServices so the things
        /// here will override registrations made in ConfigureServices.
        /// Don't build the container; that gets done for you.
        /// </summary>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule(new BusinessLogicModule
            {
                ConnectionString = Configuration.GetConnectionString("ToDoListContext")
            });

            builder.RegisterType<MessageSender>().As<IMessageSender>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseMvc();
            app.UseSwagger(options => { options.RouteTemplate = "docs/{documentName}/swagger.json"; });
            app.UseSwaggerUI(options =>
            {
                options.RoutePrefix = "docs";
                options.DocumentTitle = "ToDoList API documentation";
                options.SwaggerEndpoint("/docs/v1/swagger.json", "ToDoList API documentation V1");
            });
        }
    }
}
