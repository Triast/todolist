﻿using BusinessLogicLayer.DataTransferObjects;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace ToDoList.WebApi.Helper
{
    class MessageSender : IMessageSender
    {
        private IConfiguration _configuration;

        public MessageSender(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Add(TaskDto task)
        {
            SendMessage("POST", task);
        }

        public void Update(TaskDto task)
        {
            SendMessage("PATCH", task);
        }

        public void Remove(TaskDto task)
        {
            SendMessage("DELETE", task);
        }

        private void SendMessage(string action, TaskDto task)
        {
            var queueName = _configuration.GetValue<string>("ManagerQueue");
            var serviceId = _configuration.GetValue<string>("ServiceId");

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queueName,
                                    durable: true,
                                    exclusive: false,
                                    autoDelete: false,
                                    arguments: null);

                var message = new TaskMessage
                {
                    ServiceId = serviceId,
                    Action = action,
                    Data = JsonConvert.SerializeObject(task)
                };

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

                channel.BasicPublish(exchange: "",
                                    routingKey: queueName,
                                    basicProperties: null,
                                    body: body);
            }
        }
    }
}
