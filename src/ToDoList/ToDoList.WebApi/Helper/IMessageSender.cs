﻿using BusinessLogicLayer.DataTransferObjects;

namespace ToDoList.WebApi.Helper
{
    public interface IMessageSender
    {
        void Add(TaskDto task);
        void Update(TaskDto task);
        void Remove(TaskDto task);
    }
}
