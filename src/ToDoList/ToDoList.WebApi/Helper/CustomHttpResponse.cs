﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace TicketManagement.VenueAndUserManagement.Helpers
{
    class CustomHttpRequest
    {
        private readonly Uri _baseAddress;

        public string Token { get; set; }

        public CustomHttpRequest(string baseAddress, string token = null)
        {
            _baseAddress = new Uri(baseAddress);
            Token = token;
        }

        public async Task<T> GetRequest<T>(string uri, Action<HttpResponseMessage> onError = null)
        {
            T entity = default(T);

            using (var client = new HttpClient())
            {
                client.BaseAddress = _baseAddress;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                var response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    entity = JsonConvert.DeserializeObject<T>(json);
                }
                else
                {
                    onError?.Invoke(response);
                }
            }

            return entity;
        }

        public async Task PostRequest<T>(T model, string uri,
            Action<HttpResponseMessage> onSuccess = null,
            Action<HttpResponseMessage> onError = null)
        {
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(model);
                var data = new StringContent(json);
                data.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                client.BaseAddress = _baseAddress;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                var response = await client.PostAsync(uri, data);

                if (response.IsSuccessStatusCode)
                {
                    onSuccess?.Invoke(response);
                }
                else
                {
                    onError?.Invoke(response);
                }
            }
        }

        public async Task PutRequest<T>(T model, string uri,
            Action<HttpResponseMessage> onSuccess = null,
            Action<HttpResponseMessage> onError = null)
        {
            using (var client = new HttpClient())
            {
                var json = JsonConvert.SerializeObject(model);
                var data = new StringContent(json);
                data.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                client.BaseAddress = _baseAddress;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                var response = await client.PutAsync(uri, data);

                if (response.IsSuccessStatusCode)
                {
                    onSuccess?.Invoke(response);
                }
                else
                {
                    onError?.Invoke(response);
                }
            }
        }

        public async Task DeleteRequest(string uri, Action<HttpResponseMessage> onError = null)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = _baseAddress;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);

                var response = await client.DeleteAsync(uri);

                if (!response.IsSuccessStatusCode)
                {
                    onError?.Invoke(response);
                }
            }
        }
    }
}
