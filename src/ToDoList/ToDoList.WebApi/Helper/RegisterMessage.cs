﻿using BusinessLogicLayer.DataTransferObjects;
using System.Collections.Generic;

namespace ToDoList.WebApi.Helper
{
    public class RegisterMessage
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<TaskDto> Tasks { get; set; }
    }
}
