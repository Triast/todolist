﻿using Autofac;
using BusinessLogicLayer.Services;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public class BusinessLogicModule : Module
    {
        public string ConnectionString { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new DataAccessModule { ConnectionString = ConnectionString });

            builder.RegisterType<TaskService>().As<ITaskService>();
        }
    }
}
