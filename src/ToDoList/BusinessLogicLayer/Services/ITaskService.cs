﻿using BusinessLogicLayer.DataTransferObjects;
using System.Collections.Generic;

namespace BusinessLogicLayer.Services
{
    public interface ITaskService
    {
        TaskDto Get(int id);
        IEnumerable<TaskDto> GetAll();

        int Add(TaskDto task);
        void Remove(int id);
        void Complete(int id);
    }
}
