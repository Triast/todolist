﻿using BusinessLogicLayer.DataTransferObjects;
using DataAccessLayer.Models;
using DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogicLayer.Services
{
    internal class TaskService : ITaskService
    {
        private readonly IRepository<Task> _repo;

        public TaskService(IRepository<Task> repo)
        {
            _repo = repo;
        }

        public int Add(TaskDto task)
        {
            if (String.IsNullOrWhiteSpace(task.Text))
            {
                throw new ArgumentException("Task's text must contains letters.");
            }
            if (task.IsCompleted)
            {
                throw new ArgumentException("Task must be incompleted when adding new task");
            }

            return _repo.Add(TaskFromDto(task));
        }

        public void Complete(int id)
        {
            var dbTask = _repo.Get(id);

            if (dbTask == null)
            {
                throw new ArgumentException("Attempt to complete nonexisting task.");
            }

            dbTask.IsCompleted = true;
            _repo.Update(dbTask);
        }

        public TaskDto Get(int id) => DtoFromTask(_repo.Get(id));

        public IEnumerable<TaskDto> GetAll() => _repo.GetAll().Select(DtoFromTask);

        public void Remove(int id)
        {
            var dbTask = _repo.Get(id);

            if (dbTask == null)
            {
                throw new ArgumentException("Attempt to remove nonexisting task.");
            }

            _repo.Remove(dbTask);
        }

        private Task TaskFromDto(TaskDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Task
            {
                Id = dto.Id,
                Text = dto.Text,
                IsCompleted = dto.IsCompleted
            };
        }

        private TaskDto DtoFromTask(Task task)
        {
            if (task == null)
            {
                return null;
            }

            return new TaskDto
            {
                Id = task.Id,
                Text = task.Text,
                IsCompleted = task.IsCompleted
            };
        }
    }
}
