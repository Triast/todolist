﻿namespace BusinessLogicLayer.DataTransferObjects
{
    public class TaskDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsCompleted { get; set; }
    }
}
